﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PigeonLeague.Models
{
    public class Pigeonry
    {
        [Key,Required]
        public Guid Pigeonry_ID { get; set; }
        [DisplayName("鴿舍名稱")]
        public string Pigeonry_Name { get; set; }
        [DisplayName("所在國家")]
        public string Pigeonry_Country { get; set; }
        [DisplayName("所在城市")]
        public string Pigeonry_City { get; set; }
        [DisplayName("所在州區")]
        public string Pigeonry_state { get; set; }
        [DisplayName("所在地址")]
        public string Pigeonry_Address { get; set; }
        [DisplayName("GPS定位")]
        public string Pigeonry_Location { get; set; }

        [DisplayName("鴿舍主人")]
        public Guid Pigeonry_Owner_ID { get; set; }

        [ForeignKey("Pigeonry_Owner_ID")]
        public virtual Member Member { get; set; }

        public virtual ICollection<Pigeon> Pigeons { get; set; }
       
    }
}