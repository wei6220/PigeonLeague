﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PigeonLeague.Models
{
    public class Pigeon
    {
        [Key, Required]
        public Guid Pigeon_ID { get; set; }
        [DisplayName("賽鴿名稱")]
        public string Pigeon_Name { get; set; }
        //public string Pigeon_Owner_ID { get; set; }
        [DisplayName("賽鴿類型")]
        public string Pigeon_Type { get; set; }
        [DisplayName("賽鴿父親編號")]
        public string Pigeon_Father_ID { get; set; }
        [DisplayName("賽鴿母親編號")]
        public string Pigeon_Mother_ID { get; set; }
        [DisplayName("所屬協會")]
        public string Association_ID { get; set; }
        [DisplayName("協會腳環編號")]
        public string Association_Code { get; set; }
        [DisplayName("電子感應腳環編號")]
        public string ESFR_Code { get; set; }
        [DisplayName("擴充編號")]
        public string Extend_Code { get; set; }
        [DisplayName("賽鴿照片")]
        public string Pigeon_Pic { get; set; }

        [DisplayName("所屬鴿舍")]
        public Guid Pigeon_Pigeonry_ID { get; set; }

        [ForeignKey("Pigeon_Pigeonry_ID")]
        public virtual Pigeonry Pigeonry { get; set; }

        [NotMapped]
        public string Pigeon_Type_Name
        {
            get
            {
               var _pigeonTypeList
                 = new List<SelectListItem>() {
                    new SelectListItem { Text="灰羽",Value="1" },
                    new SelectListItem { Text="白羽",Value="2" },
                    new SelectListItem { Text="紅羽",Value="3" }
                 };
                return _pigeonTypeList.FirstOrDefault(m => m.Value == this.Pigeon_Type).Text;
            }
        }

    }
}