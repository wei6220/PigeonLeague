﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PigeonLeague.Models
{
    public class LogonViewModel
    {
        [Required, DisplayName("帳號")]
        public string Account { get; set; }

        [Required, DisplayName("密碼")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required, DisplayName("記住我")]
        public bool Remember { get; set; }

    }
}