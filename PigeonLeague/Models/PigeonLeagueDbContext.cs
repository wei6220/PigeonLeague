﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace PigeonLeague.Models
{
    public class PigeonLeagueDbContext : DbContext
    {
        public PigeonLeagueDbContext()
            : base("name=DefaultConnection")
        {
        }

        public DbSet<Member> Members { get; set; }
        public DbSet<Pigeonry> Pigeonries { get; set; }
        public DbSet<Pigeon> Pigeons { get; set; }
        public DbSet<SystemRole> SystemRoles { get; set; }
    }
}