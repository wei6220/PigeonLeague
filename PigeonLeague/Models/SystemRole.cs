﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PigeonLeague.Models
{
    public class SystemRole
    {
        [Key]
        public Guid RoleID { get; set; }

        [Required, DisplayName("群組名稱")]
        public string RoleName { get; set; }

        [Required, DisplayName("是否使用")]
        public bool IsEnable { get; set; }

        [DisplayName("更新人員")]
        public Guid? ModifyBy { get; set; }

        [DisplayName("更新時間")]
        public DateTime? ModifyOn { get; set; }
       

        public virtual ICollection<Member> Members { get; set; }

    }
}