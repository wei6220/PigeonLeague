﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PigeonLeague.Models
{
    public class Member
    {
        [Key, Required]
        public Guid Member_ID { get; set; }

        [Required, DisplayName("會員名字")]
        public string Member_FirstName { get; set; }

        [Required, DisplayName("會員姓氏")]
        public string Member_LastName { get; set; }

        [DisplayName("國家")]
        public string Member_CountryName { get; set; }

        [DisplayName("城市")]
        public string Member_CityName { get; set; }

        [DisplayName("郵遞區號")]
        public string Member_PostCode { get; set; }

        [DisplayName("地址")]
        public string Member_Address { get; set; }

        [DisplayName("電話")]
        public string Member_Tel { get; set; }

        [Required, DisplayName("手機號碼")]
        public string Member_Mobile { get; set; }

        [DisplayName("電子郵件")]
        public string Member_Email { get; set; }

        [DisplayName("會員密碼")]
        public string Account_PWD { get; set; }

        public virtual ICollection<Pigeonry> Pigeonries { get; set; }
        public virtual ICollection<SystemRole> SystemRoles { get; set; }

        //組全名用的
        [NotMapped]
        public string MemberFullName
        {
            get
            {
                return Member_FirstName + Member_LastName;
            }
        }
    }
}