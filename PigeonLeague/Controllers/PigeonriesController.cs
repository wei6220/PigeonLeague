﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PigeonLeague.Models;

namespace PigeonLeague.Controllers
{
    public class PigeonriesController : Controller
    {
        private PigeonLeagueDbContext db = new PigeonLeagueDbContext();

        // GET: Pigeonries
        public ActionResult Index()
        {
            //var pigeonries = db.Pigeonries.Include(p => p.Member);
            var pigeonries = db.Pigeonries;
            return View(pigeonries.ToList());
        }

        // GET: Pigeonries/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pigeonry pigeonry = db.Pigeonries.Find(id);
            if (pigeonry == null)
            {
                return HttpNotFound();
            }
            return View(pigeonry);
        }

        // GET: Pigeonries/Create
        public ActionResult Create()
        {
            var owners = db.Members
                        //.Where(m => m.Member_ID == null)
                        .Select(m => new
                        {
                            Member_ID = m.Member_ID,
                            Member_Name = m.Member_FirstName + m.Member_LastName
                        }).ToList();

            ViewBag.Pigeonry_Owner_ID = new SelectList(owners, "Member_ID", "Member_Name");
            return View();
        }

        // POST: Pigeonries/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Pigeonry pigeonry)
        {
            if (ModelState.IsValid)
            {
                pigeonry.Pigeonry_ID = Guid.NewGuid();
                db.Pigeonries.Add(pigeonry);
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            var owners = db.Members
                           .Select(m => new
                           {
                               Member_ID = m.Member_ID,
                               Member_Name = m.Member_FirstName + m.Member_LastName
                           }).ToList();

            ViewBag.Pigeonry_Owner_ID = new SelectList(owners, "Member_ID", "Member_Name", pigeonry.Pigeonry_Owner_ID);
            return View(pigeonry);
        }

        // GET: Pigeonries/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pigeonry pigeonry = db.Pigeonries.Find(id);
            if (pigeonry == null)
            {
                return HttpNotFound();
            }

            var owners = db.Members
                          .Select(m => new
                          {
                              Member_ID = m.Member_ID,
                              Member_Name = m.Member_FirstName + m.Member_LastName
                          }).ToList();

            ViewBag.Pigeonry_Owner_ID = new SelectList(owners, "Member_ID", "Member_Name", pigeonry.Pigeonry_Owner_ID);
            return View(pigeonry);
        }

        // POST: Pigeonries/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Pigeonry pigeonry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pigeonry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var owners = db.Members
                      .Select(m => new
                      {
                          Member_ID = m.Member_ID,
                          Member_Name = m.Member_FirstName + m.Member_LastName
                      }).ToList();

            ViewBag.Pigeonry_Owner_ID = new SelectList(owners, "Member_ID", "Member_Name", pigeonry.Pigeonry_Owner_ID);
            return View(pigeonry);
        }

        // GET: Pigeonries/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pigeonry pigeonry = db.Pigeonries.Find(id);
            if (pigeonry == null)
            {
                return HttpNotFound();
            }
            return View(pigeonry);
        }

        // POST: Pigeonries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Pigeonry pigeonry = db.Pigeonries.Find(id);
            db.Pigeonries.Remove(pigeonry);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
