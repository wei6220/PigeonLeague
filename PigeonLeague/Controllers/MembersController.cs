﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PigeonLeague.Models;

namespace PigeonLeague.Controllers
{
    public class MembersController : Controller
    {
        private PigeonLeagueDbContext db = new PigeonLeagueDbContext();

        // GET: Members
        public ActionResult Index()
        {
            return View(db.Members.ToList());
        }

        // GET: Members/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Members.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        // GET: Members/Create
        public ActionResult Create()
        {
            var roles = db.SystemRoles.Where(r => r.IsEnable).ToList();
            var items = new List<SelectListItem>();
            foreach (var role in roles)
            {
                items.Add(new SelectListItem()
                {
                    Text = role.RoleName,
                    Value = role.RoleID.ToString(),
                });
            }
            ViewBag.Roles = items;
            return View();
        }

        // POST: Members/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Member member,string[] Roles)
        {
            if (ModelState.IsValid)
            {
                member.Member_ID = Guid.NewGuid();
                member.SystemRoles = new List<SystemRole>();
                if (Roles != null)
                {
                    foreach (var role in db.SystemRoles)
                    {
                        if (Roles.Contains(role.RoleID.ToString()))
                        {
                            member.SystemRoles.Add(role);
                        }
                    }
                }

                db.Members.Add(member);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(member);
        }

        // GET: Members/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Member member = db.Members.Include(m => m.SystemRoles)
                .FirstOrDefault(m => m.Member_ID == id);

            if (member == null)
            {
                return HttpNotFound();
            }

            //var roles = db.SystemRoles.Where(r => r.IsEnable)
            //    .Select(m => new
            //    {
            //        Member_ID = m.Member_ID,
            //        Member_Name = m.Member_FirstName + m.Member_LastName
            //    }).ToList();
            var roles = db.SystemRoles.Where(r => r.IsEnable).ToList();
            var items = new List<SelectListItem>();
            foreach (var role in roles)
            {
                items.Add(new SelectListItem()
                {
                    Text = role.RoleName,
                    Value = role.RoleID.ToString(),
                    Selected = member.SystemRoles.Contains(role)
                });
            }
            ViewBag.Roles = items;

            return View(member);
        }

        // POST: Members/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Member member,string[] Roles)
        {
            if (ModelState.IsValid)
            {
                Member user = db.Members.Include(m => m.SystemRoles).FirstOrDefault(m => m.Member_ID == member.Member_ID);
                user.Member_FirstName = member.Member_FirstName;
                user.Member_LastName = member.Member_LastName;
                user.Member_Address = member.Member_Address;
                user.Member_CityName = member.Member_CityName;
                user.Member_CountryName = member.Member_CountryName;
                user.Member_Email = member.Member_Email;
                user.Member_Mobile = member.Member_Mobile;
                user.Member_PostCode = member.Member_PostCode;
                user.Member_Tel = member.Member_Tel;

                //處理role
                if (Roles != null)
                {
                    var currentRoles = user.SystemRoles.Select(x => x.RoleID);
                    foreach (var role in db.SystemRoles)
                    {
                        if (Roles.Contains(role.RoleID.ToString()))
                        {
                            if (!currentRoles.Contains(role.RoleID))
                                user.SystemRoles.Add(role);
                        }
                        else
                        {
                            if (currentRoles.Contains(role.RoleID))
                                user.SystemRoles.Remove(role);
                        }
                    }
                }
                else {
                    user.SystemRoles = new List<SystemRole>();
                }

                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(member);
        }

        // GET: Members/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Members.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        // POST: Members/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Member member = db.Members
                .Include(m => m.SystemRoles).FirstOrDefault(m => m.Member_ID == id);
           
            member.SystemRoles = null;

            db.Members.Remove(member);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
