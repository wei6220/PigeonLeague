﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PigeonLeague.Models;
using System.Web.Security;

namespace PigeonLeague.Controllers
{
    public class HomeController : Controller
    {

        PigeonLeagueDbContext db = new PigeonLeagueDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Logon()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logon(LogonViewModel logonModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var systemuser = db.Members
                .Include(x => x.SystemRoles)
                .FirstOrDefault(x => x.Member_Email == logonModel.Account);

            if (systemuser == null)
            {
                ModelState.AddModelError("", "請輸入正確的帳號或密碼!");
                return View();
            }

            //var password = CryptographyPassword(logonModel.Password, BaseController.PasswordSalt);
            //if (systemuser.Account_PWD.Equals(password))
            //{
            this.LoginProcess(systemuser, logonModel.Remember);
            return RedirectToAction("Index", "Home");
            //}
            //else
            //{
            //    ModelState.AddModelError("", "請輸入正確的帳號或密碼!");
            //    return View();
            //}
        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            //清除所有的 session
            Session.RemoveAll();

            //建立一個同名的 Cookie 來覆蓋原本的 Cookie
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            //建立 ASP.NET 的 Session Cookie 同樣是為了覆蓋
            //HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
            //cookie2.Expires = DateTime.Now.AddYears(-1);
            //Response.Cookies.Add(cookie2);

            return RedirectToAction("Index", "Home");
        }



        private void LoginProcess(Member user, bool isRemeber)
        {
            var now = DateTime.Now;
            string roles = string.Join(",", user.SystemRoles.Select(x => x.RoleName).ToArray());

            var ticket = new FormsAuthenticationTicket(
                version: 1,
                name: user.Member_Email.ToString(),
                issueDate: now,
                expiration: now.AddMinutes(30),
                isPersistent: isRemeber,
                userData: roles,
                cookiePath: FormsAuthentication.FormsCookiePath);

            var encryptedTicket = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            Response.Cookies.Add(cookie);
        }
    }
}