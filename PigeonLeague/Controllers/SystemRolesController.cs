﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PigeonLeague.Models;

namespace PigeonLeague.Controllers
{
    public class SystemRolesController : Controller
    {
        private PigeonLeagueDbContext db = new PigeonLeagueDbContext();

        // GET: SystemRoles
        public ActionResult Index()
        {
            var roles = db.SystemRoles.ToList();
            ViewBag.Members = db.Members.ToList();

            return View(roles);
        }

        // GET: SystemRoles/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemRole systemRole = db.SystemRoles.Find(id);
            if (systemRole == null)
            {
                return HttpNotFound();
            }
            ViewBag.MemberFullName = db.Members.FirstOrDefault(f => f.Member_ID == systemRole.ModifyBy).MemberFullName;

            return View(systemRole);
        }

        // GET: SystemRoles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SystemRoles/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SystemRole systemRole)
        {
            if (ModelState.IsValid)
            {
                systemRole.RoleID = Guid.NewGuid();
                systemRole.ModifyBy = Guid.Parse("F2B2A1FA-3AAC-46DB-93CA-64925A142A1D");
                systemRole.ModifyOn = DateTime.Now;

                db.SystemRoles.Add(systemRole);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(systemRole);
        }

        // GET: SystemRoles/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemRole systemRole = db.SystemRoles.Find(id);
            if (systemRole == null)
            {
                return HttpNotFound();
            }
            return View(systemRole);
        }

        // POST: SystemRoles/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SystemRole systemRole)
        {
            if (ModelState.IsValid)
            {
                systemRole.ModifyBy = Guid.Parse("F2B2A1FA-3AAC-46DB-93CA-64925A142A1D");
                systemRole.ModifyOn = DateTime.Now;
                db.Entry(systemRole).State = EntityState.Modified;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(systemRole);
        }

        // GET: SystemRoles/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemRole systemRole = db.SystemRoles.Find(id);
            if (systemRole == null)
            {
                return HttpNotFound();
            }
            return View(systemRole);
        }

        // POST: SystemRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            SystemRole systemRole = db.SystemRoles.Find(id);
            db.SystemRoles.Remove(systemRole);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
