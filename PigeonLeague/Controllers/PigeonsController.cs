﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PigeonLeague.Models;

namespace PigeonLeague.Controllers
{
    public class PigeonsController : Controller
    {
        private PigeonLeagueDbContext db = new PigeonLeagueDbContext();

        private List<SelectListItem> _pigeonTypeList 
            = new List<SelectListItem>() {
                new SelectListItem { Text="灰羽",Value="1" },
                new SelectListItem { Text="白羽",Value="2" },
                new SelectListItem { Text="紅羽",Value="3" }
            };

        // GET: Pigeons
        public ActionResult Index()
        {
            //var pigeons = db.Pigeons.Include(p => p.Pigeonry);
            var pigeons = db.Pigeons;
            //ViewBag.Pigeon_Type = new SelectList(_pigeonTypeList, "Value", "Text");
            ViewBag.Pigeon_Type = _pigeonTypeList;
            return View(pigeons.ToList());
        }

        // GET: Pigeons/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pigeon pigeon = db.Pigeons.Find(id);
            if (pigeon == null)
            {
                return HttpNotFound();
            }
            return View(pigeon);
        }

        // GET: Pigeons/Create
        public ActionResult Create()
        {
            ViewBag.Pigeon_Pigeonry_ID = new SelectList(db.Pigeonries, "Pigeonry_ID", "Pigeonry_Name");
            ViewBag.Pigeon_Type = new SelectList(_pigeonTypeList, "Value", "Text");

            return View();
        }

        // POST: Pigeons/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Pigeon_ID,Pigeon_Name,Pigeon_Type,Pigeon_Father_ID,Pigeon_Mother_ID,Association_ID,Association_Code,ESFR_Code,Extend_Code,Pigeon_Pic,Pigeon_Pigeonry_ID")] Pigeon pigeon)
        {
            if (ModelState.IsValid)
            {
                pigeon.Pigeon_ID = Guid.NewGuid();
                db.Pigeons.Add(pigeon);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Pigeon_Pigeonry_ID = new SelectList(db.Pigeonries, "Pigeonry_ID", "Pigeonry_Name", pigeon.Pigeon_Pigeonry_ID);
            return View(pigeon);
        }

        // GET: Pigeons/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pigeon pigeon = db.Pigeons.Find(id);
            if (pigeon == null)
            {
                return HttpNotFound();
            }
            ViewBag.Pigeon_Pigeonry_ID = new SelectList(db.Pigeonries, "Pigeonry_ID", "Pigeonry_Name", pigeon.Pigeon_Pigeonry_ID);
            ViewBag.Pigeon_Type = new SelectList(_pigeonTypeList, "Value", "Text", pigeon.Pigeon_Type);

            return View(pigeon);
        }

        // POST: Pigeons/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Pigeon_ID,Pigeon_Name,Pigeon_Type,Pigeon_Father_ID,Pigeon_Mother_ID,Association_ID,Association_Code,ESFR_Code,Extend_Code,Pigeon_Pic,Pigeon_Pigeonry_ID")] Pigeon pigeon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pigeon).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Pigeon_Pigeonry_ID = new SelectList(db.Pigeonries, "Pigeonry_ID", "Pigeonry_Name", pigeon.Pigeon_Pigeonry_ID);
            return View(pigeon);
        }

        // GET: Pigeons/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pigeon pigeon = db.Pigeons.Find(id);
            if (pigeon == null)
            {
                return HttpNotFound();
            }
            return View(pigeon);
        }

        // POST: Pigeons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Pigeon pigeon = db.Pigeons.Find(id);
            db.Pigeons.Remove(pigeon);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
